package com.example.mobilebankingapi.common.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize

public record EmptyJsonResponse() {
}
