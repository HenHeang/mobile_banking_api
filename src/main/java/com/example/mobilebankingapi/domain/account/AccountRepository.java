package com.example.mobilebankingapi.domain.account;

import com.example.mobilebankingapi.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
}
