package com.example.mobilebankingapi.service.user;


import com.example.mobilebankingapi.common.api.StatusCode;
import com.example.mobilebankingapi.domain.role.Role;
import com.example.mobilebankingapi.domain.role.RoleRepository;
import com.example.mobilebankingapi.domain.user.User;
import com.example.mobilebankingapi.domain.user.UserRepository;
import com.example.mobilebankingapi.exception.BusinessException;
import com.example.mobilebankingapi.mapper.UserMapper;
import com.example.mobilebankingapi.payload.user.UserRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    @Override
    public void createUser(UserRequest userRequest) {
            if (userRepository.existsByPhoneNumber(userRequest.getPhoneNumber())) {
                throw new BusinessException(StatusCode.PHONENUMBER_MORE_THEN_ONE);
            }


        User user = userMapper.fromUserCreateRequest(userRequest);
        user.setUuid(UUID.randomUUID().toString());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setImage("avatar.png");
        user.setCreateAt(LocalDateTime.now());
        user.setIsBlocked(false);
        user.setIsDeleted(false);
        user.setIsAccountNonExpired(true);
        user.setIsAccountNonLocked(true);
        user.setIsCredentialsNonExpired(true);

        // Assign default user role
        List<Role> roles = new ArrayList<>();
        Role userRole = roleRepository.findByName("USER")
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.NOT_FOUND,
                                "Role USER has not been found!"));
        roles.add(userRole);

        // set dynamic roles when create user
        userRequest.getRoles()
                .forEach(r -> {
                    Role role1 = roleRepository.findByName(r.name())
                            .orElseThrow(() -> new ResponseStatusException(
                                    HttpStatus.NOT_FOUND,
                                    "User role does not exist!"
                            ));
                    roles.add(role1);
                });

        user.setRoles(roles);

        userRepository.save(user);

//        if (userRepository.existsByPhoneNumber(userRequest.phoneNumber())) {
//            throw new ResponseStatusException(
//                    HttpStatus.CONFLICT,
//                    "Phone number has already been exited!"
//            );
//        }
//        log.info("user : {}",userRequest.toString());
////
//        if (userRepository.existsByPhoneNumber(userRequest.phoneNumber())) {
//            throw new ResponseStatusException(
//                    HttpStatus.CONFLICT,
//                    "Phone number has already been exited!"
//            );
//        }
//
//        if (userRepository.existsByNationalCardId(userRequest.nationalCardId())) {
//            throw new ResponseStatusException(
//                    HttpStatus.CONFLICT,
//                    "Nation card ID has already been exited!"
//            );
//        }
//
//        if (userRepository.existsByStudentIdCard(userRequest.studentIdCard())) {
//            throw new ResponseStatusException(
//                    HttpStatus.CONFLICT,
//                    "Student card ID has already been exited!"
//            );
//        }
//
//
//        if (!userRequest.password()
//                .equals(userRequest.comfirmedPassword())){
//            throw new ResponseStatusException(
//                    HttpStatus.BAD_REQUEST,
//                    "Password doesn't match!"
//            );
//        }
//        User user = userMapper.fromUserCreateRequest(userRequest);
//        user.setUuid(UUID.randomUUID().toString());
//        user.setPassword(passwordEncoder.encode(user.getPassword()));
//        user.setImage("avatar.png");
//        user.setCreateAt(LocalDateTime.now());
//        user.setIsBlocked(false);
//        user.setIsDeleted(false);
//        user.setIsAccountNonExpired(true);
//        user.setIsAccountNonLocked(true);
//        user.setIsCredentialsNonExpired(true);
//
//        // Assign default user role
//        List<Role> roles = new ArrayList<>();
//        com.example.mobilebankingapi.domain.role.Role userRole = roleRepository.findByName("USER")
//                .orElseThrow(() ->
//                        new ResponseStatusException(HttpStatus.NOT_FOUND,
//                                "Role USER has not been found!"));
//        roles.add(userRole);
//
//        // set dynamic roles when create user
//        userRequest.roles()
//                .forEach(r -> {
//                    com.example.mobilebankingapi.domain.role.Role role1 = roleRepository.findByName(r.name())
//                            .orElseThrow(()-> new ResponseStatusException(
//                                    HttpStatus.NOT_FOUND,
//                                    "User role does not exist!"
//                            ));
//                    roles.add(role1);
//                });
//
//
//        user.setRoles(roles);
////        System.out.println(user.toString());
//
//        userRepository.save(user);
//
//    }

    }

}
