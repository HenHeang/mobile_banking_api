package com.example.mobilebankingapi.service.user;

import com.example.mobilebankingapi.payload.user.UserRequest;

public interface UserService {
    void createUser(UserRequest userRequest);
}
