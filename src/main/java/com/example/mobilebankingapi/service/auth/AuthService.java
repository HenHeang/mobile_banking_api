package com.example.mobilebankingapi.service.auth;

import com.example.mobilebankingapi.payload.auth.AuthResponse;
import com.example.mobilebankingapi.payload.auth.LoginRequest;

public interface AuthService {
    AuthResponse login(LoginRequest loginRequest);
}
