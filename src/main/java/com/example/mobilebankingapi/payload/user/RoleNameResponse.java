package com.example.mobilebankingapi.payload.user;

public record RoleNameResponse(
        String name

) {
}
