package com.example.mobilebankingapi.payload.user;

import java.time.LocalDate;

public record UserResponse(
        String uuid,
        String name,
        String profileImage,
        String gender,
        LocalDate dob

) {
}
