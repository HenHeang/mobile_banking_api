package com.example.mobilebankingapi.payload.auth;

public record AuthResponse(
        String type,
        String accessToken,
        String refreshToken

) {
}
