//package com.example.mobilebankingapi.controller;
//
//
//import com.example.mobilebankingapi.common.api.ApiResponse;
//import com.example.mobilebankingapi.payload.auth.AuthResponse;
//import com.example.mobilebankingapi.payload.auth.LoginRequest;
//import com.example.mobilebankingapi.service.auth.AuthService;
//import jakarta.validation.Valid;
//import lombok.RequiredArgsConstructor;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/api/v1/auth")
//@RequiredArgsConstructor
//public class AuthController extends AbstractRestController{
//    private final AuthService authService;
//    @PostMapping("/login")
//    AuthResponse login(@Valid @RequestBody LoginRequest loginRequest) {
//        return authService.login(loginRequest);
//    }
//
//}
