package com.example.mobilebankingapi.mapper;


import com.example.mobilebankingapi.domain.user.User;
import com.example.mobilebankingapi.domain.user.UserAccount;
import com.example.mobilebankingapi.payload.user.UserDetailResponse;
import com.example.mobilebankingapi.payload.user.UserRequest;
import com.example.mobilebankingapi.payload.user.UserResponse;
import com.example.mobilebankingapi.payload.user.UserUpdateRequest;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User fromUserCreateRequest (UserRequest userRequest);

   UserDetailResponse toUserDetailsRespone(User user);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void fromUserUpdateRequest(UserUpdateRequest userUpdateRequest, @MappingTarget User user);

    UserResponse toUserResponse(User user);

    @Named("mapUserResponse")
    default UserResponse mapUserResponse(List<UserAccount> userAccountList){
        return toUserResponse(userAccountList.get(0).getUser());
    }


}
